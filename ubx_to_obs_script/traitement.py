#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 07/26/2021
GNSS4ENS
"""

#### Imports ###############

import configparser
import subprocess
import time
import keyboard
import os
from pathlib import Path


#### Parametres #############

rinex_ver = 1   # RINEX Version : 0 = 2.10 ; 1 = 2.11 ; 2 = 2.12 ; 3 = 3.00 ;
                # 4 = 3.01 ; 5 = 3.02 ; 6 = 3.03 ; 7 = 3.04
                
gps = True
glonass = True
galileo = False

chemin_exe = 'D:\Gnss4Ens' # Chemin du dossier contenant rtkconv.exe, rtkconv.ini, traitement.py
chemin_carte_SD = 'J:\Repertoire_Carte' # Chemin du dossier contenant les données à traiter


#### Fonctions ##############

def navigation(gps,glonass,galileo):
    """
    :param gps: selection de la constellation GPS (si True)
    :type gps: boolean
    :param glonass: selection de la constellation GLONASS (si True)
    :type glonass: boolean
    :param galileo: selection de la constellation GALILEO (si True)
    :type galileo: boolean
    :return: la cle de configuration correspondant aux constellations de GNSS placees en parametres
    :rtype: integer
    
    """
    
    if gps and glonass and galileo:
        navig = 13
    elif gps and glonass and not galileo:
        navig = 5
    elif gps and not glonass and galileo:
        navig = 9
    elif not gps and glonass and galileo:
        navig = 12
    elif not gps and not glonass and galileo:
        navig = 8
    elif not gps and glonass and not galileo:
        navig = 4
    elif gps and not glonass and not galileo:
        navig = 1
    else:
        navig = 0
    return navig


def sauv_para():
     """
     Sauvegarde les nouveaux parametres implementes dans le fichier de configuration de l'executable
    
     """
     # Enregistrement des paramètres (mode écriture)
     configfile = open('rtkconv.ini', 'w')
     config.write(configfile)
     configfile.close()
     time.sleep(1)


def lecture_exe(i):
    """
    Met a jour les parametres de configuration de RTKCONV, ouvre l'executable, lance le programme et ferme l'executable
    
    :param i:   si i = 0, le programme cherche a convertir des fichiers UBX en RINEX OBS
                si i = 1, le programme cherche a concatener des fichiers RINEX OBS
    :type i:    integer (0 ou 1)
    
    """
     
    # Mise à jour des paramètres
    if i == 0:
        config.set('set','infile',chemin_carte_SD + "\\" + repertoire + '\*.ubx') 
        config.set('set','outfile1',destination_jour + '_' + nom_stat + '\\' + repertoire + '.obs') 
    else:
        config.set('set','infile',destination_jour + '_' + nom_stat + '\*.obs') 
        config.set('set','outfile1',destination_stat + '\\' + nom_stat + '.obs') 
    
    sauv_para()
    
    # Ouverture de RTKCONV (version demo5)
    process = subprocess.Popen(r'' + chemin_exe + '\\rtkconv.exe')
    time.sleep(1)
                
    # Lancement du programme
    keyboard.press_and_release('alt+c')
    time.sleep(3)
                
    # Fermeture de l'executable lorsque la taille du fichier de sortie n'evolue plus
    if i == 0:
        chemin = destination_jour + '_' + nom_stat + '\\' + repertoire + '.obs'
    else:
        chemin = destination_stat + '\\' + nom_stat + '.obs'
    while True:
        try:
            file_size_new = Path(chemin).stat().st_size
            break
        except FileNotFoundError:
            time.sleep(2)
    file_size_old = -1
    while abs(file_size_new-file_size_old) != 0 :
        file_size_old = file_size_new
        file_size_new = Path(chemin).stat().st_size
        time.sleep(5)
    time.sleep(0.1)
    process.terminate()


if __name__ == "__main__":

    # Modification des parametres
    config = configparser.ConfigParser()
    config.read('rtkconv.ini')
    config.set('opt','rnxver',str(rinex_ver)) # Changement de la version
    config.set('opt','navsys',str(navigation(gps,glonass,galileo))) # Changement des parametres de navigation
    
    if not os.path.exists(chemin_carte_SD + '\Traitements'):
        os.makedirs(chemin_carte_SD + '\Traitements')
    
    # Repertoire des observations d'une station concaténées par jour
    destination_jour = chemin_carte_SD + '\Traitements\Obs_journalieres' 
    destination_stat = chemin_carte_SD + '\Traitements\Obs_stations' # Repertoire des observations concaténées par station
    
    if not os.path.exists(destination_stat):
        os.makedirs(destination_stat)
    
    list_noms_stat = []
    list_repertoires = os.listdir(chemin_carte_SD)
    for repertoire in list_repertoires:
        list_fichiers = os.listdir(chemin_carte_SD + "\\" + repertoire)
        
        # Si le dossier comporte des fichiers UBX
        if [fichier for fichier in list_fichiers if "ubx" in fichier]: 
            
            # Recuperation du nom de la station
            nom_stat = repertoire.split("_")[-1]
            if nom_stat not in list_noms_stat:
                list_noms_stat.append(repertoire.split("_",1)[-1])
   
    for nom_stat in list_noms_stat:
        config.set('set','format',str(5)) # Changement du format de fichier (format UBX)
        sauv_para()
        
        # Fusion des fichiers de données bruts (format UBX) pour chaque station et pour chaque jour
        if not os.path.exists(destination_jour + '_' + nom_stat):
                        os.makedirs(destination_jour + '_' + nom_stat)
                        
        for repertoire in list_repertoires:
            if nom_stat in repertoire and 'Obs_journalieres' not in repertoire:
                
                    # Ouverture, lancement et fermeture de l'executable
                    lecture_exe(0)
         
        # Fusion des observations journalières (format OBS)
        # Ouverture, lancement et fermeture de l'executable
        config.set('set','format',str(15)) # Changement du format de fichier (format RINEX)
        sauv_para()
        
        lecture_exe(1)   
        
        
        
        
        