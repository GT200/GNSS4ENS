from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.factory import Factory
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from jnius import autoclass, cast
import os
import plyer
from android import mActivity
from android.permissions import request_permissions, Permission
request_permissions([Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE])
    
class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    cancel = ObjectProperty(None)


class Root(FloatLayout):
    """
    Récupération et enregistrement en csv des données saisie dans l'interface (android)
    """
    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    # initialisation des variables stokant les 3 paramètres (nom de station, fréquence de génération de fichier ubx en minute, fréquence d'aquisition de données gnsss
    stationName = ObjectProperty(None)
    ubx_time = ""
    fr_Hz = ""
    
    def getCheckedDataTime(self, instance, state, selectedButton):
        """
        récupération du temps de génération de fichier ubx
        :param instance:
        :param state: booléen indiquant l'état du boutton radio.
        :param selectedButton: nom du bouton radio

        """
        if state:
            self.ubx_time = selectedButton

    def getCheckedDataFr(self, instance, state, selectedButton):
        """
        récupération du temps de génération de fichier ubx
        :param instance:
        :param state: booléen indiquant l'état du boutton radio.
        :param selectedButton: nom du bouton radio

        """
        if state:
            self.fr_Hz = selectedButton

    def dismiss_popup(self):
        self._popup.dismiss()


    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self._popup = Popup(title="Choose removable SD CARD", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()
        
    def save(self, sdpath):
        try:
            PythonActivity = autoclass('org.kivy.android.PythonActivity')
            Environment = autoclass('android.os.Environment')
            context = cast('android.content.Context', PythonActivity.mActivity)
            path = sdpath + "/Android/data/a.ens.gnss4ens"
            plyer.notification.notify(title='File saved in:', message=path)
            file_p = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath()
            stationName = self.stationName.text
            with open(path + '/sp.txt', 'w') as file:
                file.write ('StationID;ubx_recording_time;Frequency\n')
                file.write (stationName + ';' + self.ubx_time + ';' + self.fr_Hz)
                self.dismiss_popup()
                    
        except:
            plyer.notification.notify(title='File not saved', message="no permission to write in this storage")
        
class Gnss4ens(App):
    #def build(self):
        #return Root()
    pass

Factory.register('Root', cls=Root)
Factory.register('SaveDialog', cls=SaveDialog)


if __name__ == '__main__':
    Gnss4ens().run()
            
                
          

                
                


    
    
